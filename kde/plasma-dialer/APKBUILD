# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-dialer
pkgver=0.1_git20200829
pkgrel=0
_commit="a0f1423e8bc919d837123e4bf07ee2833f7133e6"
pkgdesc="A dialer for Plasma Mobile"
arch="all !armhf" # Blocked qt5-qtdeclarative
url="https://invent.kde.org/plasma-mobile/plasma-dialer"
license="GPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	kcoreaddons-dev
	ki18n-dev
	kpeople-dev
	kdbusaddons-dev
	knotifications-dev
	pulseaudio-dev
	telepathy-qt-dev
	libphonenumber-dev
	"
source="https://invent.kde.org/plasma-mobile/plasma-dialer/-/archive/$_commit/plasma-dialer-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="59e86967c539bd9a1a096de647be6383c1f746232ef3c555d7ed2b9b3cc14eb2294af697bd238826418b3de0bc3a4c28b62bf9a0d69155ffdedeeaa90b7d1525  plasma-dialer-a0f1423e8bc919d837123e4bf07ee2833f7133e6.tar.gz"
